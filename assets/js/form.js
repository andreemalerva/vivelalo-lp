var recaptcha = function () {
  grecaptcha.ready(function () {
    grecaptcha
      .execute("6LcH96UZAAAAAOV6xSXPoDfp3AIdZ6PCrAqHU_w_", { action: "home" })
      .then(function (token) {
        document.getElementById("g-recaptcha-response-d").value = token;
        document.getElementById("g-recaptcha-response-m").value = token;
      });
  });
};

recaptcha();
$(function () {
  /*---------------
            Validación
        ---------------*/

  $("form #telefono")
    .keypress(function (e) {
      if (isNaN(this.value + String.fromCharCode(e.charCode))) return false;
    })
    .on("cut copy paste", function (e) {
      e.preventDefault();
    });
  /*----- Habilitar boton solamente cuando este validado el formulario--*/
  $("form").on("change keyup", function () {
    var form = $(this);
    var btn = form.find("#enviar");
    let btnM = $("#enviar-m");

    if (validarFormulario(form)) {
      btn.attr("disabled", false);
      console.log("habilitado");
      btnM.attr("disabled", false);
    } else {
      btn.attr("disabled", true);
      btnM.attr("disabled", true);
      console.log("deshabilitado");
    }
  });

  var validarFormulario = function (form) {
    form.validate({
      rules: {
        nombre: { required: true },
        telefono: {
          required: true,
          digits: true,
          minlength: 10,
          maxlength: 16,
        },
        correo: { required: true, email: true },
      },

      messages: {
        nombre: { required: "El campo es obligatorio" },
        correo: {
          required: "El campo es obligatorio",
          email: "Ingresa un email válido",
        },
        telefono: {
          required: "El campo es obligatorio",
          digits: "Ingresa números solamente",
          minlength: "Mínimo {0} dígitos",
          maxlength: "Máximo {0} dí­gitos",
        },
      },
    });

    return form.valid();
  };

  $("form")
    .find("#enviar")
    .click(function (e) {
      var form = $("#form-hero");
      $("#loading-page").removeClass("d-none");
      $("#loading-page").addClass("d-flex");
      if (validarFormulario(form)) {
        var datos = form.serializeObject();
        $("#enviar").attr("disabled", "true");
        enviarExcel(datos, form);
      } else {
      }
    });

  $("form")
    .find("#enviar-m")
    .click(function (e) {
      var form = $(this.parentNode);
      $("#loading-page").removeClass("d-none");
      $("#loading-page").addClass("d-flex");
      if (validarFormulario(form)) {
        var datos = form.serializeObject();
        $("#enviar").attr("disabled", "true");
        enviarExcel(datos, form);
      } else {
      }
    });

  /*    Formato Json
        ---------------------------*/
  $.fn.serializeObject = function () {
    var o = {};
    this.find("[name]").each(function () {
      o[this.name] = this.value;
    });
    return o;
  };

  var resultado = function (form, btn, msj) {
    $("#formuario .titulo").val(msj);
    form[0].reset();
    console.log(msj);
  };

  function enviarExcel(datos, form) {
    var url =
      "https://script.google.com/macros/s/AKfycbwITHXui5edw7BEnZwfs95lZHrbkHDEOgsKYcPxtNtZoS8vjmTn/exec";
    $.ajax({
      url: url,
      method: "GET",
      data: datos,
      dataType: "JSON",
      success: function (r) {
        console.log(r);
        form.submit();
      },
      error: function (xhr) {
        console.log(xhr.status);
        console.log(xhr.responseText);
        form.submit();
      },
    });
  }

  function enviarFormulario(datos, form) {
    var url = "https://segundaferiadereclutamiento.com/heroes47/form/send";
    $.ajax({
      url: url,
      method: "POST",
      data: datos,
      dataType: "JSON",
      success: function (r) {
        console.log(r);
        window.location = window.location.origin + "/agenda-cita/gracias";
      },
      error: function (xhr) {
        console.log(xhr.status);
        console.log(xhr.responseText);
        window.location = window.location.origin + "/agenda-cita/gracias";
      },
    });
  }
});

/* OBTENER UTMS/**/

var obtener_utms = function (datos) {
  var utms = [
    "utm_source",
    "utm_medium",
    "utm_campaign",
    "utm_term",
    "utm_content",
  ];
  for (utm of utms) {
    datos[utm] = getParameterByName(utm);
  }

  return datos;
};

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null
    ? "null"
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var utms = [
  "utm_source",
  "utm_medium",
  "utm_campaign",
  "utm_term",
  "utm_content",
];

var utmFormulario = function () {
  let datos = {};
  datos = obtener_utms(datos);

  document.getElementById("utm_source").value = datos["utm_source"];
  document.getElementById("utm_content").value = datos["utm_content"];
  document.getElementById("utm_campaign").value = datos["utm_campaign"];
  document.getElementById("utm_term").value = datos["utm_term"];
  document.getElementById("utm_medium").value = datos["utm_medium"];

  document.getElementById("utm_source-m").value = datos["utm_source"];
  document.getElementById("utm_content-m").value = datos["utm_content"];
  document.getElementById("utm_campaign-m").value = datos["utm_campaign"];
  document.getElementById("utm_term-m").value = datos["utm_term"];
  document.getElementById("utm_medium-m").value = datos["utm_medium"];

  console.log(datos);
};

utmFormulario();
