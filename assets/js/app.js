/*$(document).ready(function () {
  $(".cta").on("click", function () {
    if ($(window).width() < 768) {
      $("html, body").animate(
        {
          scrollTop: $("#formulario-inferior").offset().top,
        },
        2000,
        function () {
          $("#formulario-inferior").find("#nombre").focus();
        }
      );
    } else {
      $("html, body").animate(
        {
          scrollTop: 0,
        },
        2000,
        function () {
          $("#formulario-hero").find("#nombre").focus();
        }
      );
    }
  });

  $("#proyecto").on("click", function (e) {
    e.preventDefault();

    $("html, body").animate(
      {
        scrollTop: $("#formulario-inferior").offset().top,
      },
      2000,
      function () {
        $("#formulario-inferior").find("#nombre").focus();
      }
    );
  });
}); */

$("#proyecto-menu").on("click", function (e) {
  e.preventDefault();

  $("html, body").animate(
    {
      scrollTop: $("#caracteristicas").offset().top,
    },
    2000
  );
});

$("#departamentos-menu").on("click", function (e) {
  e.preventDefault();

  $("html, body").animate(
    {
      scrollTop: $("#departamentos-s").offset().top,
    },
    2000
  );
});

$("#comercio-menu").on("click", function (e) {
  e.preventDefault();

  $("html, body").animate(
    {
      scrollTop: $("#comercio").offset().top,
    },
    2000
  );
});

$("#ubicacion-menu").on("click", function (e) {
  e.preventDefault();

  $("html, body").animate(
    {
      scrollTop: $("#ubicacion").offset().top,
    },
    2000
  );
});



/*$("#proyecto-menu").on("click", function (e) {
  e.preventDefault();

  if ($(window).width() < 768) {
    $("html, body").animate(
      {
        scrollTop: $("#hero-movil").offset().top,
      },
      2000
    );
  } else {
    $("html, body").animate(
      {
        scrollTop: $("#hero").offset().top,
      },
      2000
    );
  }
});*/
