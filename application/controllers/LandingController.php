<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Don't forget include/define REST_Controller path

/**
 *
 * Controller LandingController
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class LandingController extends CI_Controller
{
   
  public function __construct()
  {
    parent::__construct();
     $this->load->library('Phpmailer_lib');
     $this->load->model('Landing_model');

  }

  public function index()
  {
    $this->load->view('landing');
  }

  public function nuevoContacto(){
     $datos = $_POST;
    
    $estatus = array(2);
     //Validar RECAPTCHA
     $estatus[0] =  $this->validarRecaptcha($datos);
        if($estatus[0] == false){
             redirect('/gracias');
            return false;
        }
    //  //Validar datos
    $datos = $this->validarDatos($datos);
   
    //  //enviarCorreo
    $estatus[1] =  $this->enviarCorreo($datos);

    
     //Guardar en la base de datos
    // $estatus[2] = $this->guardarBaseDatos($datos);
     
     $estatus[2] = true;
    
     if($estatus[1] == true and $estatus[2] == true ){
        redirect('/gracias');
     }else{
       
        redirect('/gracias');

     }

      print_r($datos);

  }

  private function validarDatos($datos){
    foreach ($datos as $dato => $value) {
     $datos[$dato] = filter_var($value,FILTER_SANITIZE_SPECIAL_CHARS);
     $datos[$dato] = filter_var($value,FILTER_SANITIZE_URL);
     $datos[$dato] = trim($value);
     $datos[$dato] = htmlspecialchars($value);
    }

    return $datos;
  }

    private function enviarCorreo($datos){
      $fecha = date('Y-m-d\TH:i:s');
      $mensaje ="";
      $mensaje .= "<ul>";
      $campos = array("nombre","telefono","correo","utm_source","utm_medium","utm_campaign","utm_term","utm_content");
      foreach ($campos as $value) {
        $mensaje .= "<li>$value: $datos[$value]</li>";
      }
      $mensaje .= "</ul>";

       
      $mail = $this->phpmailer_lib->load();

      try{
      // $mail->SMTPDebug =2;
      $mail->isSMTP();

      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPAuth = true;
      
      $mail->Username = 'correosformularios@gmail.com';
      $mail->Password = '8j*BqMVSmP+%fD-S';

      $mail->SMTPSecure = 'ssl';
      $mail->Port = 465;

      //**//
      $mail->setFrom('correosformularios@gmail.com','Solicitud de contacto LALO');
      //develop;
      $mail->addAddress('luis.mendoza@olozfera.com'); 
      //production
      // $mail->addAddress('lespir@odg.com.mx'); 
      // $mail->addCC('luis.mendoza@olozfera.com');
      // $mail->addCC('antonio.luz@olozfera.com');
      // $mail->addCC('daniela.delagarza@olozfera.com');
      // $mail->addCC('pablo.cruz@olozfera.com');
      // $mail->addCC('rafael.cuevas@olozfera.com');

      $mail->Subject  = "Solicitud de contacto LALO";
      $mail->Body = $mensaje;
      $mail->IsHTML(true); 

      /**/
      $mail->send();

      //**//

        return true;
      }catch(Exception $exception){
            
      }

  }

  public function debugMail(){
   
    $mail = $this->phpmailer_lib->load();

    try{
    $mail->SMTPDebug =2;
    $mail->isSMTP();

    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    
    $mail->Username = 'correosformularios@gmail.com';
    $mail->Password = '8j*BqMVSmP+%fD-S';

    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;

    //**//
    $mail->setFrom('correosformularios@gmail.com');
    $mail->addAddress('luis.mendoza@olozfera.com');

    $mail->Subject  = "Test subject";
    $mail->Body = "Hola test";
    $mail->IsHTML(true); 

    /**/
    $mail->send();

    //**//


    }catch(Exception $exception){
            echo "Error: ". $exception->getMessage();
    }
  }

  private function guardarBaseDatos($datos){

    	$datos = array(
      'nombre'		=>	$datos['nombre'],
			'correo'		=>	$datos['correo'],
      'telefono'		=>	$datos['telefono'],
      'fecha'     => date('Y-m-d\TH:i:s'),
      'utm_source'=> $datos['utm_source'],
      'utm_medium'=> $datos['utm_medium'],
      'utm_campaign'=> $datos['utm_campaign'],
      'utm_content'=> $datos['utm_content'],
      'utm_term'=> $datos['utm_term']
		
    );
  
   $this->Landing_model->insertarRegistro($datos);
  
   return true;
  }

  public function validarRecaptcha($post){
   
        if(isset($post['g-recaptcha-response']) AND !empty($post['g-recaptcha-response'])){
            $captcha = $post['g-recaptcha-response'];
            $key  = "6LcH96UZAAAAAFSLcr-hGd1XQ4c0pF22s0a7KXZp";

            $ch = curl_init("https://www.google.com/recaptcha/api/siteverify?secret=".$key."&response=$captcha");
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $verify = curl_exec($ch);
            
            $respuesta = json_decode($verify);
            
            if($respuesta->success == true)
                return true;
        }

        return false;
    }

}


/* End of file LandingController.php */
/* Location: ./application/controllers/LandingController.php */