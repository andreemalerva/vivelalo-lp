<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Don't forget include/define REST_Controller path

/**
 *
 * Controller LandingController
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Base extends CI_Controller
{
   
  public function __construct()
  {
    parent::__construct();
    

  }

  public function index()
  {
        $para      = 'luis.mendoza@olozfera.com';
        $titulo    = 'El título';
        $mensaje   = 'Hola';
        $cabeceras = 'From:userolozfera@heroes47.com' . "\r\n" .
        'Reply-To: 	userolozfera@heroes47.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($para, $titulo, $mensaje, $cabeceras);
    }

}


/* End of file LandingController.php */
/* Location: ./application/controllers/LandingController.php */