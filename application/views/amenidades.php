<style>
    .amenidades-row{
        height: 510px;
    }
    .amenidades-row.col-md-6{margin-bottom: 10px;}
    .target_{ padding-right: 0px; padding-left: 0px; }

    @media(min-width: 320px) and (max-width:767px) {
        .amenidades-row.col-md-6.der{ padding-left: 0px;  padding-right: 0px; }
        .amenidades-row.col-md-6.izq{ padding-left: 0px;  padding-right:  0px; }
    }
    @media(min-width: 768px){
        .amenidades-row.col-md-6.der{ padding-left: 5px;  padding-right: 0px; }
        .amenidades-row.col-md-6.izq{ padding-left: 0px;  padding-right:  5px; }
    }
   
</style>

 <section class="row amenidades-section margin-b">
            <div class="col-12 amenidades-position">
                <h2 class="amenidades">
                    Amenidades
                </h2>
            </div>
        </section>
        <section class="row margin-target_">
            <div class="col-12 col-md-6 amenidades-row izq">
                <div  style="background: url('assets/image/web_a/lalo_alberca.jpg') no-repeat center center; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; height: 450px;">
      <!-- img src="assets/image/amenidades/img1.png" class="img-amenidades-tam" alt="Alberca semiolímpica con doble carril de nado" -->
                
                 </div>
                <h3 class="amenidades-texto amenidades-bottom">Alberca semiolímpica con doble carril de nado</h3>
            </div>
            <div class="col-12 col-md-6 amenidades-row der">
                   <div  style="background: url('assets/image/web_a/lalo_eventos.jpg') no-repeat center center; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; height: 450px;">
      <!-- img src="assets/image/amenidades/img1.png" class="img-amenidades-tam" alt="Alberca semiolímpica con doble carril de nado" -->
                
                 </div>
                <h3 class="amenidades-texto">Salón de eventos</h3>
            </div>
            <div class="col-12 col-md-6 amenidades-row izq" >
                  <div  style="background: url('assets/image/web_a/lalo_business.jpg') no-repeat center center; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; height: 450px;"></div>
                <h3 class="amenidades-texto">Business Center</h3>
            </div>
            <div class="col-12 col-md-6 amenidades-row der">
                <div  style="background: url('assets/image/web_a/lalo_terrazas.jpg') no-repeat center center; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; height: 450px;"></div>
                <h3 class="amenidades-texto">Terraza </h3>
            </div>
            <div class="col-12 col-md-6 amenidades-row izq">
                 <div  style="background: url('assets/image/web_a/lalo_juegos.jpg') no-repeat center center; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; height: 450px;"></div>
                <h3 class="amenidades-texto">Salón de juegos</h3>
            </div>
            <div class="col-12 col-md-6 amenidades-row der">
                 <div  style="background: url('assets/image/web_a/lalo_pets.jpg') no-repeat center center; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; height: 450px;"></div>
                <h3 class="amenidades-texto">Pet park</h3>
            </div>
        </section>