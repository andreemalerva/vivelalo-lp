<style>
    .navbar-nav{
        width: 800px;
        margin-left: auto;
    }
    #hero{
        margin-top: 95px;
    }
    
     #hero-movil{
        margin-top: 85px;
    }
</style>

<nav class="navbar navbar-expand-lg navbar-dark background-navbar fixed-top">
        <a class="navbar-brand ml-5" href="#"><img src="<?php echo base_url('assets/image/lalo-logo-blanco.svg'); ?>" class="brand-navbar" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#" id="proyecto-menu">Proyecto</a>
                <a class="nav-item nav-link active" href="#" id="departamentos-menu">Departamentos</a>
                <a class="nav-item nav-link active" href="#" id="comercio-menu">Comercio</a>
                <a class="nav-item nav-link active" href="#" id="ubicacion-menu">Ubicación</a>
                <!-- <a class="nav-item nav-link active" href="#" id="vive-menu">Vive y Trabaja</a> -->
                <div style="width: 80px;display: flex;">
                <a class="nav-item nav-link active" href="https://www.instagram.com/vivelalo/" target="_blank">
                    <img src="assets/image/Insta-lalo.svg" alt="logo-instagram">
                </a>
                <a class="nav-item nav-link active" href="https://www.facebook.com/vivelalo.mx" target="_blank" style="    padding-left: 24px;">
                    <img src="assets/image/fb-lalo.svg" alt="logo-face">
                </a>
                </div>
            </div>
        </div>
    </nav>