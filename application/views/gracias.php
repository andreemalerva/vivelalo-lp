<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LALO</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/lalo.css');?>" />
    <script src="https://www.google.com/recaptcha/api.js?render=6LcH96UZAAAAAOV6xSXPoDfp3AIdZ6PCrAqHU_w_"></script>
</head>

<body>

    <section class="container-fluid d-none d-md-block margin-b" id="hero">
        <div class="row he">
            <div class="col-5 d-flex flex-column align-items-center justify-content-center">
                <div class="px-5">

                    <h1 class="titulo">
                        Muchas gracias.

                    </h1>
                    <p class="texto">
                        Un concepto que retoma y proyecta un nuevo sentido de pertenencia
                        a la vida de barrio en la Loma Larga, a partir de la visión de
                        desarrollo respaldada por Proyectos 9.
                    </p>

                </div>
            </div>
            <div class="col-7" id="hero-left-container">

            </div>
        </div>
    </section>

    <section class="container-fluid d-md-none" id="hero-movil">
        <div class="row">
            <div class="col-12 p-0">
                <img src="<?php echo base_url('assets/image/movil/header-mobile.jpg');?>" alt="hero"
                    class="img-hero-movil" />
            </div>
            <div class="col-12 down-hero-movil d-flex align-items-center">
                <div class="">
                    <h1 class="titulo">
                        Muchas gracias.

                    </h1>
                    <p class="texto">
                        Un concepto que retoma y proyecta un nuevo sentido de pertenencia
                        a la vida de barrio en la Loma Larga, a partir de la visión de
                        desarrollo respaldada por Proyectos 9.
                    </p>
                </div>
            </div>
        </div>
    </section>


    <script src="<?php echo base_url('assets/js/jquery-3.4.1.js');?>">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>

</body>

</html>