<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Heroes47</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/app.css') . "?v=" . rand(0,10);?>"> 
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M2TT2MN');</script>
<!-- End Google Tag Manager -->
    
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M2TT2MN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
   
    
    <header class="container-fluid" id="hero">
        <div class="row">
            <div class="col-12 col-md-2 col-lg-3 col-xl-4 offset-md-1 mt-md-5 text-center logo-contenedor-hero">
                <img src="<?php echo base_url("assets/img/logo-hero.png");?>" alt="Logotipo Heroes47" class="img-fluid">
    
            </div>

           <div class="col-12 d-md-none hero-img-m p-0">
                <!-- <img src="assets/img/hero.jpg" alt="Logotipo Heroes47" class="movil-img"> -->
            </div>
            <!-- Seccion movil de PREVENTA DE DEPARTAMETNOS -->
            <div class="col-12 d-md-none p-0 preventa-movil-contenedor">
                <h2 class="preventa-movil"> ¡Gracias por contactarnos!</h2>
            </div>

            <!-- ******************************** -->
            <div class="contenedor-azul d-none d-md-flex justify-content-center">
                <h1 class="d-inline-block text-center"> ¡Gracias por contactarnos!</h1>
            </div>
           
        </div>
    </header>

    
     <script src="<?php echo base_url("assets/js/jquery-3.4.1.min.js");?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
</body>
</html>