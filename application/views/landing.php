<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LALO</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
    <link rel="stylesheet" href="assets/css/lalo.css" />
    <link rel="stylesheet" href="assets/css/estilos.css" />
    <!--<link rel="stylesheet" href="assets/css/lalo.css">-->
    <script src="https://www.google.com/recaptcha/api.js?render=6LcH96UZAAAAAOV6xSXPoDfp3AIdZ6PCrAqHU_w_"></script>
        <!-- Google Tag Manager -->
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-5MCBD39');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5MCBD39" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php $this->load->view('navbar'); ?>

    <section class="container-fluid d-none d-md-block margin-b" id="hero">
        <div class="row he">
            <div class="col-5 d-flex flex-column align-items-center justify-content-center">
                <div class="px-5 text-left">
                    <img src="<?php echo base_url('assets/image/logo-lalo.png'); ?>" alt="LALO" class="img-lalo" />
                    <p id="text-hola">¡lo tiene todo!</p>
                    <p class="texto">
                        Un desarrollo que retoma y proyecta un nuevo sentido de pertenencia a la Loma Larga, a partir de la visión de ciudad respaldada por Proyectos 9 + Altio Capital. </p>
                    <h1 class="titulo">
                        PREVENTA<br />
                        (DEPARTAMENTOS + COMERCIO)
                    </h1>
                    <button class="button-color-white text scroll" data-desktop="#formulario-hero">Contáctanos</button>

                </div>
            </div>
            <div class="col-7" id="hero-left-container">
            </div>
        </div>
    </section>

    <section class="container-fluid d-md-none" id="hero-movil">
        <div class="row">
            <div class="col-12 p-0">
                <img src="assets/image/hero-right-image-m.jpg" alt="hero" class="img-hero-movil" />
            </div>
            <div class="col-12 down-hero-movil d-flex align-items-center">
                <div class="">
                <img src="<?php echo base_url('assets/image/logo-lalo.png'); ?>" alt="LALO" class="img-lalo" />
                    <p id="text-hola">¡lo tiene todo!</p>
                    <p class="texto-hero-movil">
                        Un desarrollo que retoma y proyecta un nuevo sentido de pertenencia a la Loma Larga, a partir de la visión de ciudad respaldada por Proyectos 9 + Altio Capital.
                    </p>
                    <h1 class="titulo">
                        PREVENTA FASE 1<br />
                        (DEPARTAMENTOS + COMERCIO)
                    </h1>
                    <button class="button-color-white text scroll" data-mobile="#formulario-inferior">Contáctanos</button>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <section class="row margin-b" id="comunidad">
            <div class="col-12 col-md-7 comunidad-left-conteiner p-0">
                <!-- <img src="./assets//image/comunidad.jpg" alt="" class="img-fluid" /> -->
            </div>
            <div class="col-12 col-md-5 comunidad-right-container d-flex align-items-center">
                <div>
                    <div class=depto-display>
                        <h2 class="text-center comunidad-title">
                            <img src="<?php echo base_url('assets/image/lalo-logo-blue.svg'); ?>" alt="" class="logo-lalo-comunidad" />
                            <!-- <span class="es-comunidad">es...</span><br /> -->
                            <br>
                            ¡con estilo!
                        </h2>
                        <p class="comunidad-parrafo">
                            Con diseño de Rodrigo de la Peña y Gilberto L Rodríguez Arquitectos, Lalo cuenta con espacios pensados para conectar contigo y con los demás, con un diseño inteligente y elegante para todo tipo de necesidades.
                        </p>
                    </div>
                    <button class="button-blue cta scroll" data-desktop="#formulario-hero" data-mobile="#formulario-inferior">Más información</button>
                </div>
            </div>
        </section>

        <section class="row margin-b" id="departamentos-s">
            <div class="col-12 col-md-7 d-md-none text-center p-0">
            <img src="assets/image/departamentos-1.jpg"  alt="Departamentos" class="img-fluid" />
            </div>
            <div class="col-12 col-md-6 d-flex align-items-center">
                <div class="py-4 py-md-0">
                    <h2 class="departamentos-title">Departamentos</h2>
                    <p class="departamentos-text">
                        Lalo cuenta con departamentos que se adaptan a diferentes estilos de vida, desde 69m2 hasta 176m2, de 1 hasta 3 recámaras y lo mejor ¡equipados! Se entregan con closets, cocina, canceles y clima, ¡listos para vivir!
                    </p>
                    <br>
                    <p class="departamentos-text">
                        Descubre un nuevo estilo de vida, interconectado, inteligente, ¡increíble!
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6 p-0 text-center d-none d-md-block">
                <img src="assets/image/departamentos-1.jpg"  alt="Departamentos" class="img-fluid" />
            </div>
        </section>

        <section class="row margin-b" id="caracteristicas">
            <div class="col-12 col-md-3 caract-bolsa d-flex align-items-center justify-content-center p-2">
                <div class="text-center cards">
                    <img src="assets/image/iconos_deptos/bolsa_2.png" alt="icono bolsa" class="img-fluid img-carac" />
                    <h3 class="caract-bolsa__text my-2">
                        3 niveles de comercio
                    </h3>
                </div>
            </div>
            <div class="col-12 col-md-3 caract-cara d-flex align-items-center justify-content-center p-2">
                <div class="text-center cards">
                    <img src="assets/image/iconos_deptos/carita_2.png" alt="icono cara" class="img-fluid img-carac" />
                    <h3 class="my-2 caract-cara__text">
                        26 niveles de departamentos<br />
                        <span class="caract-cara__subtext">( desde 69 m2 hasta 176 m2. de 1 a 3 recámaras)</span>
                    </h3>
                </div>
            </div>
            <div class="col-12 col-md-3 caract-hotel d-flex align-items-center justify-content-center p-2">
                <div class="text-center cards">
                    <img src="assets/image/iconos_deptos/hotel_2.png" alt="icono hotel" class="img-fluid img-carac" />
                    <h3 class="my-2 caract-hotel__text">10 niveles de hotel</h3>
                </div>
            </div>
            <div class="col-12 col-md-3 caract-bolsa d-flex align-items-center justify-content-center p-2">
                <div class="text-center cards">
                    <img src="assets/image/iconos_deptos/clip_2.png" alt="icono clip" class="img-fluid img-carac" />

                    <!-- <img src="<?php echo base_url('assets/image/clip.png'); ?>" alt="clip" class="my-3 img-fluid" /> -->
                    <h3 class="caract-clip__text my-2">
                        1 nivel de Home office Suites
                    </h3>
                </div>
            </div>
            <div class="col-12">
                <button class="button-blue-text-white scroll" data-desktop="#formulario-hero" data-mobile="#formulario-inferior">Contactar a un asesor</button>
            </div>
        </section>

        <section class="row franja-img margin-b">
            <div class="col-12 franja-img-banner p-0 d-sm-none d-md-block">
            </div>
        </section>

        <section class="row margin-b" id="comercio">
        <div class="col-lg-6 col-sm-12 d-sm-block d-md-none text-center image-right-comercio-m">
            </div>
            <div class="col-lg-6 col-sm-12 text-center padding-comercio">
                <h2 class="comercio-title">Área de comercio</h2>
                <p class="comercio-texto">
                    LaLo contará con 3 niveles de área comercial con distintas zonas,
                    zona interior, terraza y bodegas. Locales desde 68.35 hasta 271.98
                    m2 en venta con escrituración tradicional. Invierte en uno de los
                    sectores más importantes de Monterrey y crece constantemente con
                    esta gran oportunidad.
                </p>
                <button class="button-white scroll" data-desktop="#formulario-hero" data-mobile="#formulario-inferior">Quiero más información</button>
            </div>
            <div class="col-lg-6 col-sm-12 d-sm-none d-md-block text-center image-right-comercio">
            </div>
        </section>

        <?php $this->load->view('amenidades'); ?>

        <section class="row" id="ubicacion">
            <div class="col-12 col-md-6 col-lg-5 p-0 mapa mapa-ubica">
                <!--<img src="assets/image/mapax.jpg" alt="mapa lalo" class="mapa d-none d-md-block" />
                <img src="assets/image/mapax-m.jpg" alt="" class="mapa d-md-none" /> -->
            </div>
            <div class="col-12 col-md-6 col-lg-7 d-flex justify-content-center align-items-center p-0 p-md-3">
                <div class="content ubi">
                    <img src="<?php echo base_url('assets/image/lalo-logo-blue.svg'); ?>" alt="" class="logo-lalo-comunidad logo-ubicacion" />
                    <h2 class="ubicacion-title text-left">
                        cerca de todo
                    </h2>
                    <div class="row clas-li-margen">
                        <div class="col-12">
                            <ul class="row ubicacion-ul">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                <li>A 5 minutos de San Pedro.</li>
                                <li>A 5 minutos de San Jerónimo.</li>
                                <li>A 6 minutos de Zona Centro.</li>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                <li>A 6 minutos de Fundidora.</li>
                                <li>A 5 minutos de Carretera Nacional.</li>
                                <li>A 30 minutos del Apto. Mariano Escobedo.</li>
                                </div>
                            </ul>
                        </div>
                    </div>
                    <div class="d-flex" id="ubicacion-text-aligh">
                        <div class="row">
                            <div class="col-12">
                                <p class="ubicacion-datos">Ubicación</p>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <p class="ubicacion-text mr-1">
                                    Ignacio Morones Prieto S/N, Col. Loma
                                    Larga 64710. Monterrey N.L
                                    <br>
                                    Entre calle Loma Larga y
                                    <br>
                                    privada Carmelita.

                                </p>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <p class="ubicacion-text">
                                    Showroom:
                                    <br>

                                    Calzada del Valle #401 LC1

                                    Col. Del Valle

                                    San Pedro Garza García
                                    <br>
                                    T: 81 1744 7406
                                    <br>
                                    info@vivelalo.mx
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="row formulario-desk margin d-none d-sm-none d-md-block">
            <div class="d-flex justify-content-center">
                <form action="<?php echo base_url('registrar'); ?>" method="POST" id="formulario-hero">
                    <div class="col-12">
                        <h3 class="title-form-hero-1">Agenda una cita</h3>
                    </div>
                    <div class="col-12">
                        <h3 class="title-form-hero">Contáctanos</h3>
                    </div>
                    <div class="col-12 form-group">
                        <input type="text" class="form-control ctr-hero" placeholder="Nombre:" id="nombre" name="nombre">
                    </div>

                    <div class="col-12 form-group">
                        <input type="text" class="form-control ctr-hero" placeholder="Email:" id="correo" name="correo">
                    </div>

                    <div class="col-12 form-group">
                        <input type="text" class="form-control ctr-hero" placeholder="Teléfono:" id="telefono" name="telefono">
                    </div>

                    <div class="col-12 form-group">
                        <select name="medio" class="form-control ctr-hero" id="medio" name="medio">
                            <option value="">¿Cómo te enteraste de Lalo?:</option>
                            <option value=" Anuncio página web"> Anuncio página web</option>
                            <option value="Asesor inmobiliario">Asesor inmobiliario</option>
                            <option value="Evento">Evento</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Google">Google</option>
                            <option value="Instagram">Instagram</option>
                            <option value="Periódico">Periódico</option>
                            <option value="Recibí un mail">Recibí un mail</option>
                            <option value="Recomendación">Recomendación</option>
                            <option value="Revista">Revista</option>
                            <option value="Valla/Cartelera">Valla/Cartelera</option>
                        </select>

                    </div>

                    <div class="col-12 form-group">
                        <div class="text-form-hero-interes">
                            <p class="text-left">Me interesa</p>
                            <label class="form-check-label label-form-hero mr-3">
                                <input class="form-check-input" type="radio" value="departamentos" id="departamentos" name="interes">Departamentos
                            </label>
                            <label class="form-check-label label-form-hero">
                                <input class="form-check-input" type="radio" value="comercial" id="comercial" name="interes"> Areá comercial
                            </label>
                        </div>
                    </div>
                    <div>
                        <input type="hidden" name="utm_source" id="utm_source">
                        <input type="hidden" name="utm_content" id="utm_content">
                        <input type="hidden" name="utm_campaign" id="utm_campaign">
                        <input type="hidden" name="utm_term" id="utm_term">
                        <input type="hidden" name="utm_medium" id="utm_medium">
                        <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response-d">
                    </div>
                    <div class="col-12 form-group">
                        <button class="button-aqua hero-radio" id="enviar" type="button">Enviar</button>
                    </div>


                </form>
            </div>
        </section>
        <section class="row d-md-none py-4" id="formulario-movil-section">
            <div class="col-12">
                <form action="<?php echo base_url('registrar'); ?>" id="formulario-inferior">
                    <div class="col-12">
                        <h3 class="title-form-hero-1">Agenda una cita</h3>
                    </div>
                    <div class="col-12">
                        <h3 class="title-form-hero">Contáctanos</h3>
                    </div>
                    <div class="col-12 form-group">
                        <input type="text" id="nombre" name="nombre" placeholder="Nombre:" class="form-control ctr-movil" />
                    </div>

                    <div class="col-12 form-group">
                        <input type="text" id="correo" name="correo" placeholder="Email:" class="form-control ctr-movil" />
                    </div>

                    <div class="col-12 form-group">
                        <input type="text" id="telefono" name="telefono" placeholder="Teléfono:" class="form-control ctr-movil" />
                    </div>

                    <div class="col-12 form-group">
                        <select name="medio" class="form-control ctr-movil" id="medio" name="medio">
                            <option value="">¿Cómo te enteraste de Lalo?:</option>
                            <option value=" Anuncio página web"> Anuncio página web</option>
                            <option value="Asesor inmobiliario">Asesor inmobiliario</option>
                            <option value="Evento">Evento</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Google">Google</option>
                            <option value="Instagram">Instagram</option>
                            <option value="Periódico">Periódico</option>
                            <option value="Recibí un mail">Recibí un mail</option>
                            <option value="Recomendación">Recomendación</option>
                            <option value="Revista">Revista</option>
                            <option value="Valla/Cartelera">Valla/Cartelera</option>
                        </select>
                    </div>

                    <div class="col-12 form-group">
                        <p class="text-form-inferior-interes">Me interesa</p>
                        <ul class="lista-form">
                            <li>

                                <label class="form-check-label label-form-inferior">
                                    <input class="form-check-input" type="radio" value="departamentos" id="departamentos" name="interes">Departamentos
                                </label>
                            </li>
                            <li>

                                <label class="form-check-label label-form-inferior">
                                    <input class="form-check-input" type="radio" value="comercial" id="comercial" name="interes"> Areá comercial
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <input type="hidden" name="utm_source" id="utm_source-m">
                        <input type="hidden" name="utm_content" id="utm_content-m">
                        <input type="hidden" name="utm_campaign" id="utm_campaign-m">
                        <input type="hidden" name="utm_term" id="utm_term-m">
                        <input type="hidden" name="utm_medium" id="utm_medium-m">
                        <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response-m">
                    </div>
                    <div class="col-12 form-group mb-4">
                        <button type="button"  class="button-aqua" id="enviar-m">Enviar</button>
                    </div>
            </div>
            </form>
    </div>
    </section>
    </div>

    <footer class="container py-4">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-6 col-md text-left col-sm-12">
                    <img src="<?php echo base_url('assets/image/footer.png'); ?>" alt="" class="img-footer" />
                </div>
                <div class="col-lg-6 col-md-auto col-sm-12 d-flex align-items-center justify-content-center py-2">
                    <a href="<?php echo base_url('aviso-de-privacidad'); ?>" target="_blank" class="aviso">Aviso de
                        privacidad</a>
                </div>
            </div>
        </div>
    </footer>

    <script src="<?php echo base_url('assets/js/jquery-3.4.1.js'); ?>">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-app.js"></script>
    <!-- TODO: Add SDKs for Firebase products that you want to use
    https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-analytics.js"></script>

    <script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-database.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="<?php echo base_url('assets/js/form.js?v=1'); ?>"></script>
    <script>
        $(function() {
            let scroll_link = $('.scroll');

            //smooth scrolling -----------------------
            scroll_link.click(function(e) {

                e.preventDefault();
                /**/
                if (window.matchMedia("(max-width: 1024px)").matches) {

                    gotoFrm = $(this).data('mobile')
                } else {

                    gotoFrm = $(this).data('desktop');
                }
                /**/

                let url = $('body').find(gotoFrm).offset().top;
                $('html, body').animate({
                    scrollTop: url
                }, 700);
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active');
                return false;
            });
        });
    </script>
</body>

</html>