<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * Model Landing_model
 *
 * This Model for ...
 * 
 * @package		CodeIgniter
 * @category	Model
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Landing_model extends CI_Model {

  // ------------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();
    $this->load->database('leads');
		$this->load->dbforge('leads');
  }

  // ------------------------------------------------------------------------


  // ------------------------------------------------------------------------
  public function index()
  {
    // 
  }

  public function insertarRegistro($datos){
    try {
      $this->db->insert("roble700_leads",$datos);
      return true;
    } catch (\Throwable $th) {
      //throw $th;
    }
  
  }

  // ------------------------------------------------------------------------

}

/* End of file Landing_model.php */
/* Location: ./application/models/Landing_model.php */