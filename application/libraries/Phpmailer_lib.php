<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * Libraries PHPMailer_Lib
 *
 * This Libraries for ...
 * 
 * @package		CodeIgniter
 * @category	Libraries
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailerException;
class PHPMailer_Lib
{
   public function __construct(){
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function load(){
        // Include PHPMailer library files
        require_once APPPATH.'third_party/PHPMailer/Exception.php';
        require_once APPPATH.'third_party/PHPMailer/PHPMailer.php';
        require_once APPPATH.'third_party/PHPMailer/SMTP.php';
        
        $mail = new PHPMailer;
        return $mail;
    }
  // ------------------------------------------------------------------------
}

/* End of file PHPMailer_Lib.php */
/* Location: ./application/libraries/PHPMailer_Lib.php */